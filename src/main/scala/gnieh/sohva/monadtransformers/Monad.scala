package gnieh.sohva.monadtransformers

import scala.language.higherKinds

// From https://medium.com/coding-with-clarity/practical-monads-dealing-with-futures-of-options-8260800712f8
// https://github.com/mattfowler/MonadTransformers by Matt Fowler
trait Monad[T[_]] {
  def map[A, B](value: T[A])(f: A => B): T[B]

  def flatMap[A, B](value: T[A])(f: A => T[B]): T[B]

  def pure[A](x: A): T[A]
}
