/*
* This file is part of the sohva project.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package gnieh.sohva

import akka.NotUsed
import strategy._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.marshalling._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.unmarshalling._
import akka.stream.{KillSwitches, Materializer, UniqueKillSwitch}
import akka.stream.scaladsl._
import akka.actor._
import akka.http.scaladsl.Http.HostConnectionPool
import akka.util.ByteString
import spray.json._

import scala.concurrent.duration._

/** A CouchDB instance.
 *  Allows users to access the different databases and information.
 *  This is the key class to start with when one wants to work with couchdb.
 *  Through this one you will get access to the databases.
 *
 *  @author Lucas Satabin
 *
 */
abstract class CouchDB {

  implicit def ec: ExecutionContext

  import SohvaProtocol._
  import SprayJsonSupport._

  implicit val system: ActorSystem
  implicit val materializer: Materializer
  // implicit val poolClientFlow //: Flow[(HttpRequest, T), (Try[HttpResponse], T), HostConnectionPool]
  // needed for the future flatMap/onComplete in the end
  // implicit val executionContext = system.dispatcher

  /** The couchDb instance host name. */
  val host: String

  /** The couchDb instance port. */
  val port: Int

  /** Whether to use ssl */
  val ssl: Boolean

  /** Returns the couchDb instance information */
  def info: Future[CouchInfo] =
    for (
      json <- http(HttpRequest(uri = uri)) withFailureMessage
        f"Unable to fetch info from $uri"
    ) yield asCouchInfo(json)

  /** Returns the database on the given couch instance. */
  def database(name: String, credit: Int = 0, strategy: Strategy = BarneyStinsonStrategy): Database =
    new Database(name, this, credit, strategy)

  /** Returns the replicator database */
  //  def replicator(name: String = "_replicator", credit: Int = 0, strategy: Strategy = BarneyStinsonStrategy): Replicator =
  //   new Replicator(name, this, credit, strategy)

  /** Returns the names of all databases in this couch instance. */
  def allDbs: Future[List[String]] =
    for (
      dbs <- http(HttpRequest(uri = uri / "_all_dbs")) withFailureMessage
        f"Unable to fetch databases list from $uri"
    ) yield asStringList(dbs)

  def dbUpdates(timeout: Option[Int] = None, heartbeat: Boolean = true): Source[DbUpdate, UniqueKillSwitch] = {

    val parameters = List(
      Some("feed" -> "continuous"),
      timeout.map(t => "timeout" -> t.toString),
      Some("heartbeat" -> heartbeat.toString)).flatten

    Source.single(prepare(HttpRequest(uri = uri / "_db_updates" <<? parameters)))
      .via(connectionFlow)
      .flatMapConcat(_.entity.dataBytes)
      .via(Framing.delimiter(ByteString("\n"), Int.MaxValue))
      .mapConcat(bs => if (bs.isEmpty) collection.immutable.Seq() else collection.immutable.Seq(JsonParser(bs.utf8String).convertTo[DbUpdate]))
      .viaMat(KillSwitches.single)(Keep.right)
  }

  /** Returns the list of nodes known by this node and the clusters.
   *  require:  Set-Cookie: AuthSession=
   *  @group CouchDB2
   *
   *  curl -fsSlv http://localhost:5984/_membership -H "Cookie: AuthSession=XXXXXXXXX"
   */
  def membership: Future[Membership] =
    for {
      mem <- http(HttpRequest(uri = uri / "_membership"))
        .withFailureMessage(f"Unable to fetch membership at $uri")
        .recover { case _ => return Future { Membership(Vector("couchdb@localhost"), Vector("couchdb@localhost")) } }
    } yield /* asMembership(mem) */ mem.convertTo[Membership]

  /** Extract Nodes names from _membership service. */
  def nodes: Future[Vector[String]] =
    membership.map(m => m.all_nodes)
      .recover {
        case _ =>
          println("No Nodes found by _membership service. Defaulting to 'couchdb@localhost' node.")
          Vector[String]("couchdb@localhost")
      }

  /** Extract first node name from _membership service. */
  def firstNode: Future[String] = {
    for { n <- nodes } yield n(0)
  }

  /** Restarts the CouchDB instance. */
  def restart: Future[Boolean] =
    for (
      resp <- http(HttpRequest(HttpMethods.POST, uri = uri / "_restart"))
        .withFailureMessage(f"Unable to restart instance at $uri")
    ) yield resp.asJsObject("ok").convertTo[Boolean]

  /*@deprecated("Use `uuid` instead", "2.0.0")
  def _uuid: Future[String] =
    uuid*/

  /** Returns one UUID */
  def uuid: Future[String] =
    for (uuid <- uuids(1))
      yield uuid.head

  /** Returns the requested number of UUIDS (by default 1). */
  def uuids(count: Int = 1): Future[List[String]] =
    for (
      uuids <- http(HttpRequest(uri = uri / "_uuids" <<? Map("count" -> count.toString))) withFailureMessage
        f"Failed to fetch $count uuids from $uri"
    ) yield asUuidsList(uuids)

  /** Returns the configuration object for this CouchDB instance */
  def config: Future[Configuration] =
    for (
      config <- http(HttpRequest(uri = uri / "_config")) withFailureMessage
        f"Failed to fetch config from $uri"
    ) yield config.convertTo[Configuration]

  /** Returns the configuration section identified by its name
   *  (an empty map is returned if the section does not exist)
   */
  def config(section: String): Future[Map[String, String]] =
    for (
      section <- http(HttpRequest(uri = uri / "_config" / section)) withFailureMessage
        f"Failed to fetch config for $section from $uri"
    ) yield section.convertTo[Map[String, String]]

  /** Returns the configuration value
   *  Returns `None` if the value does not exist
   */
  def config(section: String, key: String): Future[Option[String]] =
    for (
      section <- config(section) withFailureMessage
        f"Failed to fetch config for $section with key `$key' from $uri"
    ) yield section.get(key)

  /** Saves the given key/value association in the specified section
   *  The section and/or the key is created if it does not exist
   */
  def saveConfigValue(section: String, key: String, value: String): Future[Boolean] =
    for {
      entity <- Marshal(value.toJson).to[RequestEntity]
      res <- http(HttpRequest(HttpMethods.PUT, uri = uri / "_config" / section / key, entity = entity)) withFailureMessage
        f"Failed to save config $section with key `$key' and value `$value' to $uri"
    } yield ok(res)

  /** Deletes the given configuration key inthe specified section */
  def deleteConfigValue(section: String, key: String): Future[Boolean] =
    for (
      res <- http(HttpRequest(HttpMethods.DELETE, uri = uri / "_config" / section / key)) withFailureMessage
        f"Failed to delete config $section with key `$key' from $uri"
    ) yield ok(res)

  /** Indicates whether this couchdb instance contains the given database */
  def contains(dbName: String): Future[Boolean] =
    for (dbs <- allDbs)
      yield dbs.contains(dbName)

  /** Exposes the interface for managing couchDB users. */
  object users extends Users(this)

  // helper methods

  protected[sohva] val uri: Uri

  private lazy val http = Http()

  protected[sohva] def prepare(req: HttpRequest): HttpRequest

  protected[sohva] def rawHttp(req: HttpRequest): Future[HttpResponse] =
    http.singleRequest(prepare(req))

  // https://doc.akka.io/docs/akka-http/current/client-side/request-level.html
  // ### https://doc.akka.io/docs/akka-http/current/common/json-support.html#consuming-json-streaming-style-apis
  // https://stackoverflow.com/questions/45866019/using-akka-http-client-entity-discardentitybytes-with-strict-unmarshaller
  /** Akka update to 10.1.x -> Fix warning default-akka.actor.default-dispatcher
   *  WaitingForResponseEntitySubscription)] Response entity was not subscribed after 1 second.
   *  Make sure to read the response entity body or call `discardBytes()` on it. HEAD Empty
   *
   *  htps://github.com/akka/akka-http/issues/1836
   *  https://github.com/easel/akka-http-examples/blob/master/src/test/scala/io/github/easel/RecursiveAkkaHttpSpec.scala
   *  https://akka.io/blog/news/2018/03/08/akka-http-10.1.0-released
   */
  /*val streamingRequestFlow: Flow[HttpRequest, ByteString, NotUsed] =
    Flow[HttpRequest]
      .flatMapConcat(req ⇒
        Source.fromFuture(Http().singleRequest(req))
          .flatMapConcat { response ⇒
            response.entity.dataBytes
          }
      )*/

  protected[sohva] def http(req: HttpRequest): Future[JsValue] =
    rawHttp(req).flatMap(handleCouchResponse)

  protected[sohva] def optHttp(req: HttpRequest): Future[Option[JsValue]] =
    rawHttp(req).flatMap(handleOptionalCouchResponse)

  protected[sohva] def connectionFlow: Flow[HttpRequest, HttpResponse, Future[Http.OutgoingConnection]] =
    if (ssl)
      Http().outgoingConnectionHttps(host, port = port)
    else
      Http().outgoingConnection(host, port = port)

  // WaitingForResponseEntitySubscription Response entity was not subscribed after 1 second. Make sure to read the response entity body or call `discardBytes()` on it.
  // https://github.com/akka/akka-http/issues/1836
  // * https://github.com/akka/akka-http/issues/1836
  val streamingRequestFlow: Flow[HttpRequest, ByteString, NotUsed] =
    Flow[HttpRequest]
      .flatMapConcat(req ⇒
        Source.fromFuture(Http().singleRequest(req))
          .flatMapConcat { response ⇒
            response.entity.dataBytes
          }
      )

  private def handleCouchResponse(response: HttpResponse): Future[JsValue] =
    Unmarshal(response.entity.withContentType(ContentTypes.`application/json`)).to[JsValue].flatMap { json =>
      if (response.status.isSuccess) {
        response.discardEntityBytes()
        Future.successful(json)
      } else {
        // something went wrong...
        val code = response.status.intValue
        response.discardEntityBytes()
        val error = Try(json.convertTo[ErrorResult]).toOption
        Future.failed(CouchException(code, error))
      }
    }

  private def handleOptionalCouchResponse(response: HttpResponse): Future[Option[JsValue]] =
    handleCouchResponse(response).map(Some(_)).recoverWith {
      case CouchException(404, _) => Future.successful(None)
      case err                    => Future.failed(err)
    }

  @inline
  protected[sohva] def ok(json: JsValue) =
    json.convertTo[OkResult].ok

  @inline
  private def asCouchInfo(json: JsValue) =
    json.convertTo[CouchInfo]

  @inline
  private def asStringList(json: JsValue) =
    json.convertTo[List[String]]

  @inline
  private def asUuidsList(json: JsValue) =
    json.convertTo[Uuids].uuids

  @inline
  private def asConfiguration(json: JsValue) =
    json.convertTo[Configuration]

  @inline
  private def asMembership(json: JsValue) =
    json.convertTo[Membership]

  override def toString =
    uri.toString

}

// the different object that may be returned by the couchDb server

sealed trait DbResult

final case class OkResult(ok: Boolean, id: Option[String], rev: Option[String]) extends DbResult

final case class ErrorResult(id: Option[String], error: String, reason: String) extends DbResult

final case class CouchInfo(couchdb: String, version: String)

private[sohva] final case class Uuids(uuids: List[String])

final case class Membership(all_nodes: Vector[String], cluster_nodes: Vector[String])

final case class DbUpdate(db_name: String, seq: JsValue, `type`: String)
