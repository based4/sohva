/*
* This file is part of the sohva project.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package gnieh.sohva

import scala.concurrent.{ Await, Future }
import akka.http.scaladsl.model._
import akka.http.scaladsl.marshalling._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import gnieh.sohva.monadtransformers.{ Monad, OptionTransformer }

import scala.concurrent.duration._

/** The users database, exposing the interface for managing couchDb users.
 *
 *  Only administrators may browse list of all documents (GET /_users/_all_docs)
 *  Only administrators may listen to changes feed (GET /_users/_changes)
 *  Only administrators may execute design functions like views, shows and others
 *  There is a special design document _auth that cannot be modified
 *  Every document except the design documents represent registered CouchDB users and belong to them
 *  Users may only access (GET /_users/org.couchdb.user:Jan) or modify (PUT /_users/org.couchdb.user:Jan) documents that they own
 *  https://docs.couchdb.org/en/master/intro/security.html
 *
 *  @author Lucas Satabin
 */
class Users(couch: CouchDB) {

  import couch._

  import SohvaProtocol._
  import SprayJsonSupport._

  var dbName: String = "_users"

  private def userDb = couch.database(dbName)

  /* http://docs.couchdb.org/en/stable/api/server/common.html */
  class ClusterSetup(action: String, bind_address: String, password: String,
      port: String, singlenode: String, username: String) {
    def toRequestEntity(): RequestEntity = {
      akka.http.scaladsl.model.FormData(
        Map("action" -> action, "bind_address" -> bind_address, "password" -> password,
          "port" -> port, "singlenode" -> singlenode, "username" -> username)
      ).toEntity(HttpCharsets.`UTF-8`)
    }
  }

  /* POST http://localhost:5984/_cluster_setup
  action	enable_single_node
    bind_address	127.0.0.1
  password	admin12345
    port	5984
  singlenode	true
  username	admin */
  private def clusterSetup(clusterSetup: ClusterSetup) = {
    postFormular("_cluster_setup", clusterSetup.toRequestEntity())

    /** POST form. */
    def postFormular(path: String, form: RequestEntity): Future[Boolean] = {
      for {
        response <- couch.http(HttpRequest(HttpMethods.POST, uri = uri / path, entity = form))
          .withFailureMessage(f"Unable to get missing revisions from $uri")
      } yield ok(response)
    }
  }

  def clusterSetupSingle(bind_address: String, port: String, username: String, password: String) = {
    clusterSetup(new ClusterSetup("enable_single_node", bind_address, password, port, "true", username))
  }

  def clusterSetupMulti(bind_address: String, port: String, username: String, password: String) = {
    clusterSetup(new ClusterSetup("enable_cluster", bind_address, password, port, "false", username))
  }

  /** Adds a new user with the given role list to the user database,
   *  and returns the new instance.
   */
  def add(
    name: String,
    password: String,
    roles: List[String] = Nil): Future[Boolean] = {

    val user = CouchUser(name, password, roles)

    for {
      entity <- Marshal(user).to[RequestEntity]
      res <- http(HttpRequest(HttpMethods.PUT, uri = uri / dbName / user._id, entity = entity))
    } yield ok(res)

  }

  /** Saves the given key/value association in the specified section at a specific node
   *  The section and/or the key is created if it does not exist
   *
   *  https://stackoverflow.com/questions/42861564/how-to-get-akka-http-scaladsl-model-httprequest-body
   */
  def saveConfigValueAtNode(node: Future[Option[String]], section: String, key: String,
    value: String, isPassword: Boolean = false): Future[Boolean] = {
    // https://medium.com/coding-with-clarity/practical-monads-dealing-with-futures-of-options-8260800712f8
    // http://loicdescotte.github.io/posts/scala-compose-option-future/

    // Extract Future/Option/String: Success / Failure
    implicit val futureMonad = new Monad[Future] {
      def map[A, B](value: Future[A])(f: (A) => B) = value.map(f)

      def flatMap[A, B](value: Future[A])(f: (A) => Future[B]) = value.flatMap(f)

      def pure[A](x: A): Future[A] = Future(x)
    }

    for { n1 <- OptionTransformer(node) } yield
/// TODO fix uri path - not updated in HttpRequest
    uri.path / "_node" / n1 / "_config" / "admins" / key
    var body = value
    if (isPassword) body = "\"" + value + "\""
    for (
      res <- http(HttpRequest(HttpMethods.PUT, uri = uri,
        entity = HttpEntity(ContentTypes.`text/plain(UTF-8)`, body))) withFailureMessage
        f"Failed to set password for $key at $uri"
    ) yield ok(res)
  }

  /** Save password for a couchDB node server.
   *  path example: /_node/couchdb@localhost/_config/admins/
   *  'node' alternative value "couchdb@localhost" (first enabled by couchDB when not configured),
   *  by default use "nonode@localhost".
   */
  def savePassword(node: Future[Option[String]], login: String, pass: String): Future[Boolean] = {
    saveConfigValueAtNode(node, "admins", login, pass)
  }

  /** Save password */
  def savePassword(login: String, pass: String): Future[Boolean] =
    saveConfigValueAtNode(Future { None }, "admins", login, pass, true)

  /*   for {
       entity <- Marshal(value.toJson).to[RequestEntity]
       res <- http(HttpRequest(HttpMethods.PUT, uri = uri / "_config" / section / key, entity = entity)) withFailureMessage
         f"Failed to save config $section with key `$key' and value `$value' to $uri"
     } yield ok(res)*/

  /** Deletes the given user from the database. */
  def delete(name: String): Future[Boolean] =
    database(dbName).deleteDoc("org.couchdb.user:" + name)

  override def toString =
    userDb.toString

}
