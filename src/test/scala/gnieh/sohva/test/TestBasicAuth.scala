/*
* This file is part of the sohva project.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package gnieh.sohva
package test

import org.scalatest._

// http://www.beyondthelines.net/computing/scala-future-and-execution-context/
// the following is equivalent to `implicit val ec = ExecutionContext.global`
//import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

// http://docs.couchdb.org/en/stable/api/database/security.html#api-db-security
class TestBasicAuth extends SohvaTestSpec with BeforeAndAfterAll {

  val username = "test-basic"
  val password = "test-basic"

  override def beforeAll(): Unit = try {
    super.beforeAll()
  } finally {
    // add a user
    val userDb = session.database("_users")

    // get nodes name (membership)
    val membership = session.membership // to fix not completed

    val stream = new java.io.ByteArrayOutputStream()
    Console.withErr(stream) {
      //all printlns in this block will be redirected
      // https://stackoverflow.com/questions/47723475/mixing-futures-and-vectors-in-a-for-comprehension
      // https://viktorklang.com/blog/Futures-in-Scala-protips-2.html
      // https://stackoverflow.com/questions/20905569/synchronize-a-computation-of-2-and-more-futures
      // http://mushtaq.github.io/blog/2013/03/09/synchronizing-scala-futures-comparing-styles/ flow
      println("Nodes:")
      println("\tall_nodes\t\tcluster_nodes")

      for (
        (nodes, clusters) <- membership.map(m => (m.all_nodes, m.cluster_nodes))
      ) yield { println(nodes, clusters) }

      println("*Nodes:")
      membership.map(m => m.all_nodes foreach println)
        .recover { case _ => "Empty membership.all_nodes" }

      println("Clusters:")
      membership.map(m => m.cluster_nodes foreach println)
        .recover { case _ => "Empty membership.cluster_nodes" }

    }

    synced(new Users(session).savePassword(username, password))

    /* synced(userDb.saveDoc(
      CouchUser(
        username,
        password,
        List()))) */
  }

  override def afterAll(): Unit = try {
    val userDb = session.database("_users")
    synced(userDb.deleteDoc("org.couchdb.user:" + username))
  } finally {
    super.afterAll()
  }

  "A basic session" should "give access to user document" in {

    val basicSession = couch.startBasicSession(username, password)

    val basicUser = synced(basicSession.currentUser)

    basicUser should be('defined)

    val basicUserDb = basicSession.database("_users")
    val basicRev = synced(basicUserDb.getDocRevision("org.couchdb.user:" + username))

    basicRev should be('defined)

  }

}
