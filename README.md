Gnieh Sohva
===========

[![Join the chat at https://gitter.im/gnieh/sohva](https://badges.gitter.im/gnieh/sohva.svg)](https://gitter.im/gnieh/sohva?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

Sohva is a set of Scala tools for [CouchDB](http://couchdb.apache.org/) databases.
The main tool is a client library, making it possible to store and retrieve documents in CouchDB.

Using Sohva
-----------

Sohva is built against [Scala](https://www.scala-lang.org/) 2.12 with CouchDB 2.3.0.

Have a look at the [website](http://sohva.gnieh.org/) for more documentation

On github - [https://github.com/gnieh/sohva](https://github.com/gnieh/sohva)
==========================================
[![Build Status](https://travis-ci.org/gnieh/sohva.png?branch=master)](https://travis-ci.org/gnieh/sohva)

