#!/bin/bash
USER=${COUCHDB_USERNAME:-admin}
PASS=${COUCHDB_PASSWORD:-admin12345}
DB=${COUCHDB_DBNAME:-mydb}

# Start CouchDB service
# /usr/local/bin/couchdb -b
/opt/couchdb/bin/couchdb -b
while ! nc -vz 127.0.0.1 5984; do sleep 1; done

# Create User
echo "Creating user: \"$USER\"..."
curl -X PUT http://127.0.0.1:5984/_config/admins/$USER -d '"'${PASS}'"'

# Create Database
if [ ! -z "$DB" ]; then
    echo "Creating database: \"$DB\"..."
    curl -X PUT http://$USER:$PASS@127.0.0.1:5984/$DB
fi

# Stop CouchDB service
# /usr/local/bin/couchdb -d
/opt/couchdb/bin/couchdb

echo "========================================================================"
echo "CouchDB User: \"$USER\""
echo "CouchDB Password: \"$PASS\""
if [ ! -z "$DB" ]; then
    echo "CouchDB Database: \"$DB\""
fi
echo "========================================================================"

rm -f /.firstrun
