# Build

chmod g+rx docker-entrypoint.sh

docker build -t couchdb_image .

docker images -a

mkdir -p mycouchdb

// docker run -d -v /webroot:/var/www/html -p 80:80 --name hakase nginx_image

// docker run --rm -ti -d -v //TEST/alpine://data alpine ls -R /data

// docker run --rm -ti -d -v "D:\Tools\Docker Toolbox\couchdb\mycouchdb" -e "COUCHDB_USER=admin" -e "COUCHDB_PASSWORD=admin12345" -p 5984:5984 -d couchdb_image

docker run --name mycouchdb \
           -p 5984:5984 \
           -v mycouchdb:/opt/couchdb/data:Z \
           -e "COUCHDB_USER=admin" -e "COUCHDB_PASSWORD=admin12345" \
           -d couchdb_image
// 2>&1 1>/dev/null | docker logs

// https://stackoverflow.com/questions/1507816/with-bash-how-can-i-pipe-standard-error-into-another-process      

// bash4: 2>&1   

docker start -a mycouchdb

https://blogs.msdn.microsoft.com/stevelasker/2016/06/14/configuring-docker-for-windows-volumes/

https://forums.docker.com/t/volume-mounts-in-windows-does-not-work/10693/89

# Check

docker ps -a

docker network ls

docker network inspect <network name>

https://runnable.com/docker/basic-docker-networking

docker info

# Logs

docker logs mycouchdb

# Debug

docker events&

docker logs <copy the instance id from docker events messages on screen>

https://serverfault.com/questions/596994/how-can-i-debug-a-docker-container-initialization

# Remove

docker rm name_of_the_docker_container

https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes

docker rmi -f couchdb_image

docker images purge

## all

docker rmi -f $(docker images -a -q)

https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes#removing-docker-images

# Errors
1) [FATAL tini (6)] exec /docker-entrypoint.sh failed: Permission denied
https://github.com/composer/docker/issues/7

Two suffixes :z or :Z can be added to the volume mount. 
These suffixes tell Docker to relabel file objects on the shared volumes. 
The 'z' option tells Docker that the volume content will be shared between containers. 
Docker will label the content with a shared content label. 
Shared volumes labels allow all containers to read/write content. 
The 'Z' option tells Docker to label the content with a private unshared label. 
https://stackoverflow.com/questions/35218194/what-is-z-flag-in-docker-containers-volumes-from-option

2) https://forums.docker.com/t/error-in-docker-image-creation-invoke-rc-d-policy-rc-d-denied-execution-of-restart-start/880
RUN echo exit 0 > /usr/sbin/policy-rc.d

https://github.com/Microsoft/WSL/issues/3583

3) debconf: delaying package configuration, since apt-utils is not installed 
https://github.com/phusion/baseimage-docker/issues/319
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils

4) + apt-key list
   Warning: apt-key output should not be parsed (stdout is not a terminal)
https://stackoverflow.com/questions/48162574/how-to-circumvent-apt-key-output-should-not-be-parsed

Setting up libssl1.0.2:amd64 (1.0.2r-1~deb9u1) ...
Processing triggers for libc-bin (2.24-11+deb9u4) ...
Setting up xz-utils (5.2.2-1.2+b1) ...
update-alternatives: using /usr/bin/xz to provide /usr/bin/lzma (lzma) in auto mode
update-alternatives: warning: skip creation of /usr/share/man/man1/lzma.1.gz because associated file /usr/share/man/man1/xz.1.gz (of link group lzma) doesn't exist

+ gpg --batch --verify /usr/local/bin/tini.asc /usr/local/bin/tini
gpg: Signature made Sat Apr 21 07:42:13 2018 UTC
gpg:                using RSA key 0B588DFF0527A9B7
gpg: Good signature from "Thomas Orozco <thomas@orozco.fr>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.

+ gpg --batch --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 8756C4F765C9AC3CB6B85D62379CE192D401AB61
gpg: keybox '/tmp/tmp.27s4Te0ybv/pubring.kbx' created
gpg: keyserver receive failed: Cannot assign requested address


Get:1 http://cdn-fastly.deb.debian.org/debian stretch/main amd64 wget amd64 1.18-5+deb9u2 [799 kB]
debconf: unable to initialize frontend: Dialog
debconf: (TERM is not set, so the dialog frontend is not usable.)
debconf: falling back to frontend: Readline
debconf: unable to initialize frontend: Readline
debconf: (Can't locate Term/ReadLine.pm in @INC (you may need to install the Term::ReadLine module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.24.1 /usr/local/share/perl/5.24.1 /usr/lib/x86_64-linux-gnu/perl5/5.24 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl/5.24 /usr/share/perl/5.24 /usr/local/lib/site_perl /usr/lib/x86_64-linux-gnu/perl-base .) at /usr/share/perl5/Debconf/FrontEnd/Readline.pm line 7, <> line 1.)
debconf: falling back to frontend: Teletype
dpkg-preconfigure: unable to re-open stdin: 


5) 
gpg: keyserver receive failed: No data
> https://github.com/balena-io/etcher/issues/1786

...
+ gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu
gpg: Signature made Mon Oct 15 22:44:22 2018 UTC
gpg:                using RSA key B42F6819007F00F88E364FD4036A9C25BF357DD4
gpg: Can't check signature: No public key


+ rmdir /var/lib/couchdb /var/log/couchdb
rmdir: failed to remove '/var/lib/couchdb': Directory not empty

open_result error {not_found,no_db_file} for _users

Error in process <0.343.0> with exit value:
{database_does_not_exist,[{mem3_shards,load_shards_from_db,"_users",[{file,"src/mem3_shards.erl"},{line,395}]},{mem3_shards,load_shards_from_disk,1,[{file,"src/mem3_shards.erl"},{line,370}]},{mem3_shards,load_shards_from_disk,2,[{file,"src/mem3_shards.erl"},{line,399}]},{mem3_shards,for_docid,3,[{file,"src/mem3_shards.erl"},{line,86}]},{fabric_doc_open,go,3,[{file,"src/fabric_doc_open.erl"},{line,39}]},{chttpd_auth_cache,ensure_auth_ddoc_exists,2,[{file,"src/chttpd_auth_cache.erl"},{line,195}]},{chttpd_auth_cache,listen_for_changes,1,[{file,"src/chttpd_auth_cache.erl"},{line,142}]}]}

chttpd_auth_cache changes listener died database_does_not_exist at mem3_shards:load_shards_from_db/6(line:395) <= mem3_shards:load_shards_from_disk/1(line:370) <= mem3_shards:load_shards_from_disk/2(line:399) <= mem3_shards:for_docid/3(line:86) <= fabric_doc_open:go/3(line:39) <= chttpd_auth_cache:ensure_auth_ddoc_exists/2(line:195) <= chttpd_auth_cache:listen_for_changes/1(line:142)

# Documentation
https://www.howtoforge.com/tutorial/how-to-create-docker-images-with-dockerfile/#step-create-dockerfile

https://hub.docker.com/_/couchdb

http://127.0.0.1:5984/_utils/#setup/singlenode

# Notes
Gosu: 고수 'Expert' in Korean. 코리안 https://en.wikipedia.org/wiki/Gosu