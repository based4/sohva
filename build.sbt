import scalariform.formatter.preferences._

val scala211 = "2.11.12"
val scala212 = "2.12.8"
val scala213 = "2.13.0-M5"

lazy val commonSettings = Seq(
  organization := "org.gnieh",
  version := "3.0.0-SNAPSHOT",
  scalaVersion := scala212,
  licenses += ("The Apache Software License, Version 2.0" -> url("http://www.apache.org/licenses/LICENSE-2.0.txt")),
  homepage := Some(url("https://github.com/gnieh/sohva")),
  scmInfo := Some(ScmInfo(url("https://github.com/gnieh/sohva"), "git@github.com:gnieh/sohva.git")),
  crossScalaVersions := Seq("2.12.3", "2.11.8"),
  libraryDependencies ++= globalDependencies,
  parallelExecution := false,
  scalariformAutoformat := true,
  scalariformPreferences := {
    scalariformPreferences.value
      .setPreference(AlignSingleLineCaseStatements, true)
      .setPreference(DoubleIndentConstructorArguments, true)
      .setPreference(MultilineScaladocCommentsStartOnFirstLine, true)
  },
  fork in Test := true,
  scalacOptions ++= Seq("-deprecation", "-feature"),
  scalacOptions in (Compile, doc) ++= Seq("-groups")) ++ Seq(
    scalariformPreferences := {
      scalariformPreferences.value
        .setPreference(AlignSingleLineCaseStatements, true)
        .setPreference(DoubleIndentConstructorArguments, true)
        .setPreference(MultilineScaladocCommentsStartOnFirstLine, true)
        .setPreference(DanglingCloseParenthesis, Prevent)
    }) ++ publishSettings

lazy val scalariform = Seq(
  scalariformAutoformat := true,
  scalariformPreferences :=
    scalariformPreferences.value
      .setPreference(AlignSingleLineCaseStatements, true)
      .setPreference(DoubleIndentConstructorArguments, true)
      .setPreference(DanglingCloseParenthesis, Preserve)
      .setPreference(MultilineScaladocCommentsStartOnFirstLine, true)
)

lazy val globalDependencies = Seq(
  "org.scalatest" %% "scalatest" % "3.0.4" % "test",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.7",
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.21",
  "org.gnieh" %% "diffson-spray-json" % "2.2.6", // 3.1.0 https://github.com/gnieh/diffson/releases
  "io.spray" %% "spray-json" % "1.3.4",
  "org.slf4j" % "slf4j-api" % "1.7.25"
)

lazy val publishSettings = Seq(
  publishMavenStyle := true,
  publishArtifact in Test := false,
  // The Nexus repo we're publishing to.
  publishTo := Some(
  if (isSnapshot.value)
    Opts.resolver.sonatypeSnapshots
  else
    Opts.resolver.sonatypeStaging
  ),
  pomIncludeRepository := { x => false },
  pomExtra := (
    <scm>
      <url>https://github.com/gnieh/sohva</url>
      <connection>scm:git:git://github.com/gnieh/sohva.git</connection>
      <developerConnection>scm:git:git@github.com:gnieh/sohva.git</developerConnection>
      <tag>HEAD</tag>
    </scm>
    <developers>
      <developer>
        <id>satabin</id>
        <name>Lucas Satabin</name>
        <email>lucas.satabin@gnieh.org</email>
      </developer>
    </developers>
    <ciManagement>
        <system>travis</system>
        <url>https://travis-ci.org/#!/gnieh/sohva</url>
      </ciManagement>
      <issueManagement>
        <system>github</system>
        <url>https://github.com/gnieh/sohva/issues</url>
      </issueManagement>
    )
  )

lazy val sohva = project.in(file("."))
  .dependsOn(json % "compile-internal, test-internal")
  .enablePlugins(SiteScaladocPlugin, JekyllPlugin, SbtOsgi, GhpagesPlugin)
  .settings(commonSettings)
  .settings(publishSettings)
  .settings(osgiSettings)
  .settings(scalariform)
  .settings (
    name := "sohva",
    description := "Couchdb client library",
    fork in test := true,
    mappings in (Compile, packageBin) ++= mappings.in(json, Compile, packageBin).value,
    mappings in (Compile, packageSrc) ++= mappings.in(json, Compile, packageSrc).value,
    sources in (Compile, doc) ++= sources.in(json, Compile, doc).value,
    com.typesafe.sbt.site.jekyll.JekyllPlugin.autoImport.requiredGems := Map(
        "jekyll" -> "3.3.0"),
    git.remoteRepo := scmInfo.value.get.connection,
    resourceDirectories in Compile := List(),
    OsgiKeys.exportPackage := Seq(
      "gnieh.sohva",
      "gnieh.sohva.*"
    ),
    OsgiKeys.additionalHeaders := Map (
      "Bundle-Name" -> "Sohva CouchDB Client"
    ),
    OsgiKeys.bundleSymbolicName := "org.gnieh.sohva",
    OsgiKeys.privatePackage := Seq())

lazy val json = project.in(file("sohva-json"))
  .settings(commonSettings)
  .settings(scalariform)
  .settings(
    libraryDependencies ++= globalDependencies,
    libraryDependencies += scalaVersion("org.scala-lang" % "scala-reflect" % _).value)
